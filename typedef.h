#ifndef TYPEDEF_H
#define TYPEDEF_H




typedef unsigned char 			tdu1;
typedef signed char 			tds1;
typedef char		 			tdc1;
typedef unsigned short 			tdu2;
typedef signed short 			tds2;
typedef unsigned int 			tdu4;
typedef signed int 				tds4;
typedef float 					tdf4;
typedef double 					tdf8;
typedef int 					tboo;
typedef unsigned char			tdbl;
typedef unsigned long long 		tdu8;
typedef signed long long 		tds8;


#endif // TYPEDEF_H

