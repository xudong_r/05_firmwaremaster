#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QSerialPort>
#include <QStringList>
#include <QTime>
#include <QByteArray>
#include "comprotocol.h"
#include "typedef.h"
#include <QLibrary>

#define DEVICE_ID               0x1F
#define DEVICE_NAME             "DG3"


#define BLFRM_CMD_DWPARA 			(0x21)  //下载APP程序的配置参数
#define BLFRM_CMD_RDPARA 			(0x22)  //读取APP程序的配置信息
#define BLFRM_CMD_DWBLPA 			(0x23)  //下载Bootloader程序的参数
#define BLFRM_CMD_RDBLPA 			(0x24)  //读取Bootloader程序的参数
#define BLFRM_CMD_DWPRG  			(0x31)  //下载APP程序文件
#define BLFRM_CMD_DWBL   			(0x32)  //下载BootLoader程序文件
#define BLFRM_CMD_RUNAPP 			(0x50)  //运行APP程序
#define BLFRM_CMD_ERASE  			(0x60)  //擦除Flash


namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();
private slots:

    void on_btnOpenSerialPort_clicked();
    void on_btnSearchSerialPort_clicked();
    void onSerialReadyRead();
    void onSerialBytesWritten(qint64 size);

    void on_pbChooseFile_clicked();

    void on_pbUploadProgram_clicked();

    void on_pbStartApp_clicked();

    void on_pbAESEncrypt_clicked();

    void on_pbAESDecrypt_clicked();

    void on_pbAESDecryptChooseFile_clicked();

    void on_pbCheckFile_clicked();

    void on_pbAESEncryptChooseFile_clicked();

    void on_pbJumpToBootLoader_clicked();

private:

    void initUI();
    void initSerialPort();
    void enableEditableUI(bool enable);
    // 初始化用户软件
    void init_UserSw();
    void BootMessagePacked(QByteArray* Bytes);
    tdu1 BootMessageUnpacked(QByteArray* Bytes);
    Ui::MainWidget *ui;
    QTimer* timerSN;
    QTimer* timerActive;    
    QTimer* timerSendData;
    QSerialPort *mpSerialPort;
    tdu1 SerialRxFlag;//串口接收状态机
    // 串口设置
    QString             mComPort;
    int                 mBaud;
    int                 mDataBits;
    int                 mParity;
    int                 mStopBits;
    bool                mStarted;
    QStringList         mSLSerialPorts;         // 当前串口列表
    QStringList         mProgramSystemType;     // 程序设备类型列表
    QByteArray          mDataGram;
    int                 mLastCurTemp;


    QTimer*             mpTimer;




    //串口接收数据解析协议
    COMPROTOCOL *SerialResolveProtocol;
    void SerialSendData(tdu1);
    void SerialSendData(tdu1,QByteArray,tdu1);


    void ReceiveVersion(QByteArray Bytes);
    void ReceiveSN(QByteArray Bytes);
    void ReceiveAck(QByteArray Bytes);
    void ReceiveCompileTime(QByteArray Bytes);
    void ReceiveDownLoadAppData(QByteArray Bytes);

public slots:
    void COMRxCmd(tZDLinkData* ZDdata);



};

#endif // MAINWIDGET_H
