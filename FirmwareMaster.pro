#-------------------------------------------------
#
# Project created by QtCreator 2015-10-13T14:45:55
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += debug_and_release
CONFIG(debug, debug|release) {
	TARGET = FirmwareMaster_Debug
} else {
	TARGET = FirmwareMaster
    DEFINES += PRODUCT_RELEASE QT_NO_DEBUG_OUTPUT
#    LIBS      += -L$$_PRO_FILE_PWD_/ -lSoftwareLicense
}

TEMPLATE = app

DESTDIR = $$_PRO_FILE_PWD_/


SOURCES += main.cpp \
    mainwidget.cpp \
    crc.cpp \
    comprotocol.cpp \
    cmdstruct.cpp \
    aes.cpp

HEADERS  += \
    mainwidget.h \
    typedef.h \
    crc.h \
    comprotocol.h \
    cmdstruct.h \
    softwarelicense.h \
    aes.h

FORMS    += \
    mainwidget.ui



DISTFILES +=
