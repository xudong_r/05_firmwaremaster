
#include <QFileDialog>
#include <QFile>
#include "mainwidget.h"
#include "ui_mainwidget.h"
#include "qMessageBox.h"
#include "qserialportinfo.h"
#include "comprotocol.h"
#include <QTimer>
#include <QTextStream>
#include <stdio.h>
#include "aes.h"
#include "crc.h"
#include <QDateTime>
typedef void (*FUN1)(tdu1*, tdu1*);
QTextStream cin(stdin,  QIODevice::ReadOnly);

QTextStream cout(stdout,  QIODevice::WriteOnly);

QTextStream cerr(stderr,  QIODevice::WriteOnly);

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    mpSerialPort = NULL;
    mComPort = "";
    mBaud = QSerialPort::Baud115200;
    mDataBits = QSerialPort::Data8;
    mParity = QSerialPort::NoParity;
    mStopBits = QSerialPort::OneStop;
    mStarted = false;
    mpTimer = NULL;
    timerSN = new QTimer();
    timerSN->setSingleShot(true);

    timerActive = new QTimer();
    timerActive->setSingleShot(true);

    timerSendData = new QTimer();
    timerSendData->setSingleShot(true);


    //QLibrary AheadXAccreditLib()

    ui->setupUi(this);
    initUI();


    //设置串口协议指针
    SerialResolveProtocol = new COMPROTOCOL();
    initSerialPort();

    QObject::connect(SerialResolveProtocol,SIGNAL(COMRxCmd(tZDLinkData*)),this,SLOT(COMRxCmd(tZDLinkData*)));


    //初始化用户软件
    init_UserSw();
}

MainWidget::~MainWidget()
{
    if(mpSerialPort!=NULL)
    {
        delete mpSerialPort;
    }
    delete ui;
}

void MainWidget::COMRxCmd(tZDLinkData *ZDdata)
{
    switch(ZDdata->ID)
    {
        case BLFRM_CMD_DWPRG:
        {
            this->ReceiveDownLoadAppData(ZDdata->Data);
            break;
        }
        default:
            break;
    }
}


void MainWidget::on_btnOpenSerialPort_clicked()
{
    if (mStarted) {
        if (mpSerialPort == NULL) {
            return;
        }
        if (mpSerialPort->isOpen ()) {
            mpSerialPort->close();
        }
        ui->cbSerial->setEnabled(true);
        ui->btnOpenSerialPort->setText("打开");
        ui->btnSearchSerialPort->setEnabled(true);
        mStarted = false;
    } else {
        if (ui->cbSerial->currentIndex() >= 0){
            QString port = mSLSerialPorts.at(ui->cbSerial->currentIndex());
            if (mpSerialPort == NULL) {
#ifdef Q_OS_WIN
                mComPort = port;
#else
                mComPort = "/dev/" + port;
#endif
                mpSerialPort = new QSerialPort(mComPort);
                connect(mpSerialPort, SIGNAL(readyRead()), this, SLOT(onSerialReadyRead()));
                connect(mpSerialPort, SIGNAL(bytesWritten(qint64)), this, SLOT(onSerialBytesWritten(qint64)));
            } else if (mComPort.compare(port) != 0) {
                delete mpSerialPort;
#ifdef Q_OS_WIN
                mComPort = port;
#else
                mComPort = "/dev/" + port;
#endif
                mpSerialPort = new QSerialPort(mComPort);
                connect(mpSerialPort, SIGNAL(readyRead()), this, SLOT(onSerialReadyRead()));
                connect(mpSerialPort, SIGNAL(bytesWritten(qint64)), this, SLOT(onSerialBytesWritten(qint64)));
            }
            mpSerialPort->setBaudRate (mBaud);
            mpSerialPort->setDataBits ((QSerialPort::DataBits)(mDataBits));
            mpSerialPort->setParity ((QSerialPort::Parity)(mParity));
            mpSerialPort->setStopBits ((QSerialPort::StopBits)(mStopBits));
            if (!mpSerialPort->open (QSerialPort::ReadWrite)) {
                QString msg = QString("Fail to open serialport(%1).").arg (mComPort);
                QMessageBox::critical(this, "Error", msg, QMessageBox::Ok);
                return;
            }
            ui->cbSerial->setEnabled(false);
        } else {
            QMessageBox::warning(this, "错误", "没有发现串口！");
            return;
        }
        ui->btnOpenSerialPort->setText("关闭");
        ui->btnSearchSerialPort->setEnabled(false);
        mStarted = true;
    }
}

void MainWidget::on_btnSearchSerialPort_clicked()
{
    initSerialPort();

}

void MainWidget::onSerialReadyRead()
{
    QByteArray data = mpSerialPort->readAll();
    if (!data.isEmpty()) {
        // 从串口读到数据
        qDebug("Serail read: %d", data.length());
        SerialResolveProtocol->COMRxData(data);
    }
}

void MainWidget::onSerialBytesWritten(qint64 size)
{
    qDebug("Serial write: %lld", size);
}

void MainWidget::initUI()
{
  //初始化UI界面

}

void MainWidget::initSerialPort()
{
    QStringList slPorts;
    // 搜索串口
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        slPorts << info.portName();
    }

    if (slPorts.isEmpty()) {
        //未找到串口
        mSLSerialPorts.clear();
        ui->cbSerial->clear();
        return;
    }
    if (mSLSerialPorts.isEmpty()) {
        //串口列表为空
        mSLSerialPorts = slPorts;
        ui->cbSerial->addItems(mSLSerialPorts);
        ui->cbSerial->setCurrentIndex(0);
    }
    else {
        //串口列表不为空

        //获取当前串口
        QString curPort = mSLSerialPorts.at(ui->cbSerial->currentIndex());
        //下拉列表清空
        ui->cbSerial->clear();
        //获取当前串口在串口列表中的位置
        int index = slPorts.indexOf(curPort);

        mSLSerialPorts = slPorts;
        ui->cbSerial->addItems(mSLSerialPorts);
        if (index > -1) {
            ui->cbSerial->setCurrentIndex(index);
        } else {
            ui->cbSerial->setCurrentIndex(0);
        }
    }
}




//串口发送数据
void MainWidget::SerialSendData(tdu1 CmdID)
{
    if(mpSerialPort)
    {
        if(mpSerialPort->isOpen())
        {
            mpSerialPort->write((char*)SerialResolveProtocol->SerialSendData(CmdID).data(),7);
        }else
        {
            QMessageBox::warning(this,"错误","请先打开串口");
        }
    }else
    {
        QMessageBox::warning(this,"错误","请先打开串口");
    }
}

//串口发送数据
void MainWidget::SerialSendData(tdu1 CmdID,QByteArray Data,tdu1 DataLength)
{
    if(mpSerialPort)
    {
        if(mpSerialPort->isOpen())
        {

        mpSerialPort->write((char*)SerialResolveProtocol->SerialSendData(CmdID,Data,DataLength).data(),DataLength+7);
        }else
        {
            QMessageBox::warning(this,"错误","请先打开串口");
        }
    }else
    {
         QMessageBox::warning(this,"错误","请先打开串口");
    }
}



//版本信息结构体定义
typedef struct
{
    tdu2 SysType;
    tdu1 Major;
    tdu1 Minor;
    tdu1 Revision;
}tVersion;

//时间信息结构体
typedef struct
{
    tdu2 Year;  	//年 2010
    tdu1 Month; 	//月 1-12
    tdu1 Day;   	//日 1-31
    tdu1 Hour;    	//时 0-23
    tdu1 Minute;    //分 0-59
    tdu1 Second;    //秒 0-59
}tTime;

//程序信息结构体
typedef struct
{
    tVersion PackageVer;    // 打包方式版本
    tdu4 ProgramSize;       // 程序代码大小，单位字节
    tVersion ProgramVer;    // 程序版本
    tTime ProgramTime;       // 程序文件时间
    tdu4 ProgramCRC32;      // 程序文件CRC32校验,指对原始bin文件进行CRC32校验的值
    tdu1 ProgramAESKey[16];
    tdu2 CRC16;             // 此结构体首字节到CRC16前一个字节校验
}tProgramMessage;


#define BOT_MESSAGE_LENGTH          (256)   //BootLoader文件信息数据长度
#define BOOTLOADER_PACKAGE_LENGTH   (128)   //BootLoader传输数据包最大长度

//初始化用户软件
void MainWidget::init_UserSw()
{
    mProgramSystemType << "未知设备";
    mProgramSystemType << "解码器";
    mProgramSystemType << "设备3";
    mProgramSystemType << "设备4";
    ui->cbProgramSysType->addItems(mProgramSystemType);
    ui->cbProgramSysType_Decrypt->addItems(mProgramSystemType);
    ui->cbProgramSysType_Version->addItems(mProgramSystemType);
    ui->pbUploadProgram->setEnabled(false);
    ui->cbSerial->setCurrentIndex(0);
}
static tdu1 FileAESKey[16] = {0x09, 0x7C, 0x1A, 0xD7, 0x24, 0xB0, 0x5E, 0x3B, 0x9A, 0x76, 0x67, 0xD3, 0x34, 0x1E, 0x65, 0x04};   //文件密钥
static tdu1 ProgAESKey[16] = {0xA3, 0xB6, 0x20, 0xC8, 0xBA, 0xCD, 0xF1, 0xC8, 0x71, 0xBD, 0x8C, 0x0A, 0xBB, 0x95, 0x74, 0x7B};   //固件密钥(Bin文件),固件传输密钥
static tdu1 TranAESKey[16] = {0xE9, 0x73, 0xB7, 0xED, 0x05, 0xC4, 0xD7, 0x65, 0x88, 0xAB, 0x28, 0xFC, 0xF3, 0x99, 0x90, 0x7B};   //参数信息传输密钥

//解析解密后文件信息
tProgramMessage DecrpytProgMessage;
tdu1 MainWidget::BootMessageUnpacked(QByteArray* Bytes)
{
    //此处应该先校验在拷贝数据

    tdu1 i = 0;
    //打包版本
    memcpy((void*)&DecrpytProgMessage.PackageVer.SysType, (void*)&Bytes->data()[i], sizeof(tdu2));
    i += sizeof(tdu2);
    memcpy((void*)&DecrpytProgMessage.PackageVer.Major, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);
    memcpy((void*)&DecrpytProgMessage.PackageVer.Minor, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);
    memcpy((void*)&DecrpytProgMessage.PackageVer.Revision, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);


    //程序大小
    memcpy((void*)&DecrpytProgMessage.ProgramSize, (void*)&Bytes->data()[i], sizeof(tdu4));
    i += sizeof(tdu4);

    //程序版本
    memcpy((void*)&DecrpytProgMessage.ProgramVer.SysType, (void*)&Bytes->data()[i], sizeof(tdu2));
    i += sizeof(tdu2);
    memcpy((void*)&DecrpytProgMessage.ProgramVer.Major, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);
    memcpy((void*)&DecrpytProgMessage.ProgramVer.Minor, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);
    memcpy((void*)&DecrpytProgMessage.ProgramVer.Revision, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);

    //程序加密时间
    memcpy((void*)&DecrpytProgMessage.ProgramTime.Year, (void*)&Bytes->data()[i], sizeof(tdu2));
    i += sizeof(tdu2);
    memcpy((void*)&DecrpytProgMessage.ProgramTime.Month, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);
    memcpy((void*)&DecrpytProgMessage.ProgramTime.Day, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);
    memcpy((void*)&DecrpytProgMessage.ProgramTime.Hour, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);
    memcpy((void*)&DecrpytProgMessage.ProgramTime.Minute, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);
    memcpy((void*)&DecrpytProgMessage.ProgramTime.Second, (void*)&Bytes->data()[i], sizeof(tdu1));
    i += sizeof(tdu1);

    //程序CRC32
    memcpy((void*)&DecrpytProgMessage.ProgramCRC32, (void*)&Bytes->data()[i], sizeof(tdu4));
    i += sizeof(tdu4);

    //程序AES密钥
    memcpy((void*)&DecrpytProgMessage.ProgramAESKey[0], (void*)&Bytes->data()[i], sizeof(tdu1) * 16);
    i += (sizeof(tdu1) * 16);

    //根据i的长度计算CRC16
    tdu2 crc16 = CRC::CRC_AheadX((tdu1 *)&Bytes->data()[0], i);

    //结构体信息CRC16
    memcpy((void*)&DecrpytProgMessage.CRC16, (void*)&Bytes->data()[i], sizeof(tdu2));
    i += sizeof(tdu2);

    tdu1 res = 0;
    if(crc16 == DecrpytProgMessage.CRC16)
    {
        if(CRC::make_crc32((tdu1 *)&Bytes->data()[BOT_MESSAGE_LENGTH], DecrpytProgMessage.ProgramSize) == DecrpytProgMessage.ProgramCRC32)
        {
            res = 0;
        }
        else
            res = 1;
    }
    else
        return 1;

    return res;
}

//选择固件
static tdu1 FileCheck = 0xFF;  // 0: 检查通过,1：检查未通过
void MainWidget::on_pbChooseFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("选择固件"),
                                                    "/home",
                                                    tr("RIN files (*.rin)"));
    FileCheck = 1;
    ui->leFirmwarePath->setText(fileName);
}

//检查文件按键
QByteArray DecrpytFileBuf;
void MainWidget::on_pbCheckFile_clicked()
{
    tdu4 fileSize = 0;

    QFile file(ui->leFirmwarePath->text());
    //打开文件
    if (!file.open(QIODevice::ReadOnly))
        return;
    QByteArray content = file.readAll();
    file.close();

    //获取文件长度
    fileSize = content.size();
    DecrpytFileBuf.resize(fileSize);

    //拷贝需要解密的文件到数组
    for(tdu4 i = 0; i < (tdu4)content.size(); i++)
    {
        DecrpytFileBuf.data()[ i] = content.data()[i];
    }

    //解密
    AES_SetKey((char*)&FileAESKey[0], 16);
    AES_Decrypt((char*)&DecrpytFileBuf.data()[0], (tds4)fileSize);

    //bin文件二次解密
    AES_SetKey((char*)&ProgAESKey[0], 16);
    AES_Decrypt((char*)&DecrpytFileBuf.data()[BOT_MESSAGE_LENGTH], (tds4)(fileSize - BOT_MESSAGE_LENGTH));

    if(BootMessageUnpacked(&DecrpytFileBuf))
    {
        //解包错误
        FileCheck = 1;
        ui->pbUploadProgram->setEnabled(false);
    }
    else
    {
        //校验通过显示文件信息
        cout << DecrpytProgMessage.ProgramTime.Year << endl;
        ui->cbProgramSysType_Decrypt->setCurrentIndex(DecrpytProgMessage.ProgramVer.SysType);
        ui->leProgramVersion_Major_Decrypt->setText(QString::number(DecrpytProgMessage.ProgramVer.Major));
        ui->leProgramVersion_Minor_Decrypt->setText(QString::number(DecrpytProgMessage.ProgramVer.Minor));
        ui->leProgramVersion_Revision_Decrypt->setText(QString::number(DecrpytProgMessage.ProgramVer.Revision));
        ui->pbUploadProgram->setEnabled(true);
        FileCheck = 0;
    }

}

tdu1 GetProgramMessageLength(void)
{
    tdu1 length = 0;

    //PackageVer
    length += sizeof(tdu2);
    length += sizeof(tdu1);
    length += sizeof(tdu1);
    length += sizeof(tdu1);

    //ProgramSize
    length += sizeof(tdu4);

    //ProgramVer
    length += sizeof(tdu2);
    length += sizeof(tdu1);
    length += sizeof(tdu1);
    length += sizeof(tdu1);

    //ProgramTime
    length += sizeof(tdu2);
    length += sizeof(tdu1);
    length += sizeof(tdu1);
    length += sizeof(tdu1);
    length += sizeof(tdu1);
    length += sizeof(tdu1);

    //ProgramCRC32
    length += sizeof(tdu4);

    //ProgramAESKey
    length += sizeof(tdu1) * 16;

    //CRC16
    length += sizeof(tdu2);

    return length;
}

//上传固件按键
void MainWidget::on_pbUploadProgram_clicked()
{
    if(FileCheck == 0)
    {
        //检测到有效文件，可以传输，发送文件信息
        QByteArray Bytes;
        tdu1 messgageLength = GetProgramMessageLength();

        //为加密调整缓冲区长度
        tdu1 dataLength = (messgageLength % 16) ? ((messgageLength / 16 + 1) * 16) : messgageLength;
        //计入传输密钥长度
        dataLength += 16;
        Bytes.resize(dataLength);
        //拷贝密钥到缓冲区
        memcpy((void*)&Bytes.data()[0], (void*)&TranAESKey[0], 16);
        //拷贝数据到缓冲区
        memcpy((void*)&Bytes.data()[16], (void*)&DecrpytFileBuf.data()[0], messgageLength);

        AES_SetKey((char*)&TranAESKey[0], 16);
        AES_Encrypt((char*)&Bytes.data()[16], (tds4)(dataLength - 16));

        SerialSendData(BLFRM_CMD_DWPARA, Bytes, dataLength);

        //进度条清零
        ui->procB_UploadDaata->setValue(0);
    }
    else
    {
        //未检测到有效固件
        QMessageBox::warning(this,"错误","未检测到有效固件");
    }

}

//固件上传回应
void MainWidget::ReceiveDownLoadAppData(QByteArray Bytes)
{
    tdu2 packID = 0;
    tdu1 packLength = 0;
    tdu4 packNum = 0;

    //获取总包数
    if((DecrpytFileBuf.size() - BOT_MESSAGE_LENGTH) % BOOTLOADER_PACKAGE_LENGTH == 0)
        packNum = (DecrpytFileBuf.size() - BOT_MESSAGE_LENGTH) / BOOTLOADER_PACKAGE_LENGTH;
    else
        packNum = (DecrpytFileBuf.size() - BOT_MESSAGE_LENGTH) / BOOTLOADER_PACKAGE_LENGTH + 1;

    //获取回传的上一包的packID
    memcpy((void*)&packID,(void*)&Bytes.data()[0],2);

    //判断是不是最后一包
    if(packID == packNum - 1)
    {
        ui->procB_UploadDaata->setValue(100);
        QMessageBox::warning(this,"成功","升级成功 点击启动 运行程序");
        return;
    }

    switch(Bytes.data()[2])
    {
        case 0x00:
        {
            // 正确收到上一包数据，准备下一包数据，packID++
            packID++;
            break;
        }
        case 0x01:
        {
            //未能正确写入或接收上一包数据
            break;
        }
        case 0x02:
        {
            //擦出错误
            QMessageBox::warning(this,"错误","错误代码：0x02");
            return;
        }
        case 0x03:
        {
            //写入APP参数失败
            QMessageBox::warning(this,"错误","错误代码：0x03");
            return;
        }
        case 0x04:
        {
            //校验APP参数失败
            QMessageBox::warning(this,"错误","错误代码：0x04");
            return;
        }
        default:
        {
            QMessageBox::warning(this,"错误","错误代码：0xFF");
            return;
        }
    }

    if((packID + 1) * 128 < DecrpytFileBuf.size() - BOT_MESSAGE_LENGTH)
        //剩余数据长大于128字节，发送128字节
        packLength = 128;
    else
        //发送数据小于128字节，发送剩余字节
        packLength = DecrpytFileBuf.size() - BOT_MESSAGE_LENGTH - packID * 128;

    //进度条
    ui->procB_UploadDaata->setValue(packID * 128 * 100 / (DecrpytFileBuf.size() - BOT_MESSAGE_LENGTH));

    //打包缓冲区
    QByteArray Bytes1;
    Bytes1.resize(2 + packLength);
    //前两个字节是packID
    memcpy((void*)&Bytes1.data()[0],(void*)&packID,2);
    memcpy((void*)&Bytes1.data()[2],(void*)&DecrpytFileBuf.data()[packID * 128 + BOT_MESSAGE_LENGTH],packLength);

    //此处AES算法输入数据的长度为16的倍数条件，由文件时保证，因此不做限制
    AES_SetKey((char*)&ProgAESKey[0], 16);
    AES_Encrypt((char*)&Bytes1.data()[2], (tds4)packLength);
    SerialSendData(BLFRM_CMD_DWPRG, Bytes1, packLength + 2);
}

//设备启动
void MainWidget::on_pbStartApp_clicked()
{
    QByteArray Bytes;
    Bytes.resize(1);
    Bytes.data()[0] = 0;
    SerialSendData(BLFRM_CMD_RUNAPP, Bytes, 1);
}

//选择加密文件路径按键
void MainWidget::on_pbAESEncryptChooseFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("选择固件"),
                                                    "/home",
                                                    tr("Bin files (*.bin)"));

    ui->leAESEncryptFilePath->setText(fileName);
}

//程序信息打包到加密文件
void MainWidget::BootMessagePacked(QByteArray* Bytes)
{
    tProgramMessage progMessage;
    tdu1 i = 0;
    //打包版本
    progMessage.PackageVer.SysType = 1;
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.PackageVer.SysType, sizeof(tdu2));
    i += sizeof(tdu2);
    progMessage.PackageVer.Major = 2;
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.PackageVer.Major, sizeof(tdu1));
    i += sizeof(tdu1);
    progMessage.PackageVer.Minor = 3;
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.PackageVer.Minor, sizeof(tdu1));
    i += sizeof(tdu1);
    progMessage.PackageVer.Revision = 1;
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.PackageVer.Revision, sizeof(tdu1));
    i += sizeof(tdu1);


    //程序大小 Bin文件大小 单位:字节
    QString filePath = ui->leAESEncryptFilePath->text();
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly))
        return;
    QByteArray content = file.readAll();
    file.close();
    progMessage.ProgramSize = content.size();
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramSize, sizeof(tdu4));
    i += sizeof(tdu4);

    //程序版本
    bool res;
    progMessage.ProgramVer.SysType = ui->cbProgramSysType->currentIndex();
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramVer.SysType, sizeof(tdu2));
    i += sizeof(tdu2);
    progMessage.ProgramVer.Major = QString(ui->leProgramVersion_Major->text()).toUShort(&res);
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramVer.Major, sizeof(tdu1));
    i += sizeof(tdu1);
    progMessage.ProgramVer.Minor = QString(ui->leProgramVersion_Minor->text()).toUShort(&res);
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramVer.Minor, sizeof(tdu1));
    i += sizeof(tdu1);
    progMessage.ProgramVer.Revision = QString(ui->leProgramVersion_Revision->text()).toUShort(&res);
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramVer.Revision, sizeof(tdu1));
    i += sizeof(tdu1);

    //程序加密时间
    QDateTime current_date_time =QDateTime::currentDateTime();
    progMessage.ProgramTime.Year = current_date_time.date().year();
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramTime.Year, sizeof(tdu2));
    i += sizeof(tdu2);
    progMessage.ProgramTime.Month = current_date_time.date().month();
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramTime.Month, sizeof(tdu1));
    i += sizeof(tdu1);
    progMessage.ProgramTime.Day = current_date_time.date().day();
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramTime.Day, sizeof(tdu1));
    i += sizeof(tdu1);
    progMessage.ProgramTime.Hour = current_date_time.time().hour();
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramTime.Hour, sizeof(tdu1));
    i += sizeof(tdu1);
    progMessage.ProgramTime.Minute = current_date_time.time().minute();
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramTime.Minute, sizeof(tdu1));
    i += sizeof(tdu1);
    progMessage.ProgramTime.Second = current_date_time.time().second();
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramTime.Second, sizeof(tdu1));
    i += sizeof(tdu1);

    //程序CRC32
    progMessage.ProgramCRC32 = CRC::make_crc32((tdu1 *)&Bytes->data()[BOT_MESSAGE_LENGTH], progMessage.ProgramSize);
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramCRC32, sizeof(tdu4));
    i += sizeof(tdu4);

    //程序AES密钥
    memcpy((void*)&progMessage.ProgramAESKey[0], (void*)&ProgAESKey[0], sizeof(tdu1) * 16);
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.ProgramAESKey[0], sizeof(tdu1) * 16);
    i += (sizeof(tdu1) * 16);

    //结构体信息CRC16
    progMessage.CRC16 = CRC::CRC_AheadX((tdu1 *)&Bytes->data()[0], i);
    memcpy((void*)&Bytes->data()[i], (void*)&progMessage.CRC16, sizeof(tdu2));
    i += sizeof(tdu2);

}

//加密按键
void MainWidget::on_pbAESEncrypt_clicked()
{
    tdu4 fileSize = 0;//记录文件大小，最大4GB-1Byte
    QString filePath = ui->leAESEncryptFilePath->text();
    QFile file(filePath);

    if (!file.open(QIODevice::ReadOnly))
        return;
    QByteArray content = file.readAll();
    file.close();

    if(content.size() > 10485760)
    {
        QMessageBox::warning(this,"错误","请先打开串口");
        return;
    }
    //保证文件的长度为16的倍数
    if(content.size() % 16)
        fileSize = (content.size() / 16 + 1) * 16;
    else
        fileSize = content.size();

    fileSize += BOT_MESSAGE_LENGTH;

    QByteArray DataBuf;
    DataBuf.resize(fileSize);

    //拷贝文件到数据
    for(tdu4 i = 0; i < (tdu4)content.size(); i++)
    {
        DataBuf.data()[BOT_MESSAGE_LENGTH + i] = content.data()[i];
    }

    //打包文件信息到数组
    BootMessagePacked(&DataBuf);

    //先对原始文件进行一次加密
    AES_SetKey((char*)&ProgAESKey[0], 16);
    AES_Encrypt((char*)&DataBuf.data()[BOT_MESSAGE_LENGTH], (tds4)fileSize - BOT_MESSAGE_LENGTH);

    //加密
    AES_SetKey((char*)&FileAESKey[0], 16);
    AES_Encrypt((char*)&DataBuf.data()[0], (tds4)fileSize);

    //改变文件路径
    for(tdu1 i = 0; i < filePath.size(); i++)
    {
        if(filePath.data()[i] == '.')
        {
            filePath.resize(i + 1 + 3);
            filePath.data()[++i] = 'r';
            filePath.data()[++i] = 'i';
            filePath.data()[++i] = 'n';
            break;
        }
    }

    //写入到新文件
    QFile botFile(filePath);
    botFile.open(QIODevice::WriteOnly);
    botFile.write(DataBuf);
    botFile.close();
}

//选择解密文件路径按键
void MainWidget::on_pbAESDecryptChooseFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("选择固件"),
                                                    "/home",
                                                    tr("RIN files (*.rin)"));
    ui->leAESDecryptFilePath->setText(fileName);
}

//解密按键
void MainWidget::on_pbAESDecrypt_clicked()
{
    tdu4 fileSize = 0;//记录文件大小，最大4GB-1Byte
    QString filePath = ui->leAESDecryptFilePath->text();
    QFile file(filePath);

    //打开文件
    if (!file.open(QIODevice::ReadOnly))
        return;
    QByteArray content = file.readAll();
    file.close();

    //获取文件长度
    fileSize = content.size();

    QByteArray DataBuf;
    DataBuf.resize(content.size());

    //拷贝需要解密的文件到数组
    for(tdu4 i = 0; i < (tdu4)content.size(); i++)
    {
        DataBuf.data()[ i] = content.data()[i];
    }

    //解密
    AES_SetKey((char*)&FileAESKey[0], 16);;
    AES_Decrypt((char*)&DataBuf.data()[0], (tds4)fileSize);

    //改文件名
    for(tdu1 i = 0; i < filePath.size(); i++)
    {
        if(filePath.data()[i] == '.')
        {
            filePath.resize(i + 1 + 3);
            filePath.data()[++i] = 'b';
            filePath.data()[++i] = 'i';
            filePath.data()[++i] = 'n';
            break;
        }
    }

    //定义一个新的区域来存放解密文件
    QByteArray DataBufOut;
    DataBufOut.resize(content.size() - BOT_MESSAGE_LENGTH);
    for(tdu4 i = 0; i < (tdu4)content.size() - BOT_MESSAGE_LENGTH; i++)
    {
         DataBufOut.data()[i] = DataBuf.data()[i + BOT_MESSAGE_LENGTH];
    }

    AES_SetKey((char*)&ProgAESKey[0], 16);;
    AES_Decrypt((char*)&DataBufOut.data()[0], (tds4)DataBufOut.size());

    //将文件存入新的路径
    QFile botFile(filePath);
    botFile.open(QIODevice::WriteOnly);
    botFile.write(DataBufOut);
    botFile.close();
}

void MainWidget::on_pbJumpToBootLoader_clicked()
{
    tdu1 JumpToBootLoader[18] = {0xEB, 0x90, 0x03, 0x00, 0x01, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0xE0, 0x01, 0x01, 0x02, 0xA1};
    if(mpSerialPort)
    {
        if(mpSerialPort->isOpen())
        {
            mpSerialPort->write((char*)&JumpToBootLoader[0], 18);
        }else
        {
            QMessageBox::warning(this,"错误","请先打开串口");
        }
    }else
    {
         QMessageBox::warning(this,"错误","请先打开串口");
    }
}


























