#ifndef CRC_H
#define CRC_H
#include "typedef.h"
#include <QByteArray>
class CRC
{
public:
    CRC();
    static tdu2 CRC_AheadX(tdu1* pBytes,tdu4 Length);
    static tdu2 CRC_Standard(tdu1* pBytes,tdu4 Length);
    static uint32_t make_crc32(unsigned char *string, uint32_t size);
};

#endif // CRC_H
