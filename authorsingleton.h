#ifndef AUTHORSINGLETON_H
#define AUTHORSINGLETON_H

#include <QLibrary>

class AuthorSingleton
{
private:
    AuthorSingleton();
public:
    static AuthorSingleton& instance();
    virtual ~AuthorSingleton();
    bool checkLicense(bool deep);
    int checkSum();
    int checkNum() const {
        return mCheckNum;
    }
private:
    QString dllPathname();
    bool initLibrary();
    int fcsTypeValue(int type);

    typedef SoftwareLicense* (*FUNC_CREATEOBJECT)(int, int);
    typedef void (*FUNC_RELEASEOBJECT)(SoftwareLicense*);
    // 授权相关
    QLibrary*               mpLibLicense;
    SoftwareLicense*        mpSoftLicense;
    FUNC_CREATEOBJECT       mpFuncCreate;
    FUNC_RELEASEOBJECT      mpFuncRelease;
    unsigned int            mSlValue1;
    unsigned int            mSlValue2;
    unsigned int            mSlLevel;
    int                     mCheckNum;
};

#endif // AUTHORSINGLETON_H
